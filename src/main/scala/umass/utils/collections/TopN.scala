package umass.utils.collections

import scala.collection.mutable.ArrayBuffer
import scala.util.Random

/**
 * Created by jshankar on 9/28/14.
 */

// for top-N elements, use the minimum heap
class TopN[A](maxLength: Int) {

    private val _pq = new  collection.mutable.PriorityQueue[(A, Int)]()(dis)

    // assume elem cannot be added more than twice, we need indexed-pq
    def add(elem: A, count: Int): Unit = {

          if (_pq.size < maxLength ) {
               _pq += (elem -> count)
          } else if (count > _pq.head._2 ) {
            _pq.dequeue
            _pq += (elem -> count)
          }
    }

    def +=(elem: A, count: Int) = {
      add(elem, count)
    }

    def getTop() = _pq.head._1

    def getTopN() = {
           val ans = new ArrayBuffer[(A, Int)]
           var i = 0
           while (!_pq.isEmpty) {
              ans += _pq.head
             _pq.dequeue
           }
           ans.reverse
    }

    private def dis() = new Ordering[(A, Int)] {
          def compare(a: (A, Int), b: (A, Int)) = -a._2.compare(b._2)
    }
}

object TopNTest {
     def main(args: Array[String]): Unit = {
         val x = new TopN[String](10)
         val rng = new Random
         for (i <- 0 until 30) {
            x += (i.toString, rng.nextInt(100))
         }
         for (ele <- x.getTopN())
             println(ele)
     }
}
