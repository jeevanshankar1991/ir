package umass.ir.p1

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

class CorpusStat {
  var n_books  = 0
  var n_pages  = 0
  var n_words  = 0
}

class BookStat(nme: String = "") {
  var name        = nme
  var n_words     = 0
  var n_uni_words = 0
  var n_pages     = 0
  var pages       = new ArrayBuffer[PageStat]

}

class PageStat(nme: String = "") {
  var name        = nme
  var n_words     = 0
  var n_uni_words = 0
}

class SingleWordStat(count: Int = 0) {
  var cnt     = count
  var n_pages = 0
  var n_books = 0
  var n_next  = 0
  var window_stat: collection.mutable.HashMap[Int, WordPairStat] = null


}

class WordPairStat {
  var n_window = 0
  var n_page   = 0
  var n_after  = 0
  var n_before = 0
}


class VocabStat(vocabFile: String, specialwordsFile: String, stopwordsFile: String, minCount: Int = 5) {

  val vocab             = new mutable.HashMap[String, Int] // maps the string to its rank
  val rev_vocab         = new mutable.HashMap[Int, String] // maps the rank to its string
  val word_stat         = new ArrayBuffer[SingleWordStat]  // creates an array of WordStat
  val ans               = loadVocabFile(vocabFile)
  val total_words       = ans._1
  val total_uni_words   = ans._2
  val specialwords      = new mutable.HashSet[Int]
  val stopwords         = new mutable.HashSet[Int]
  io.Source.fromFile(specialwordsFile).getLines.map(getId(_)).filter(_ != -1 ).foreach(specialwords += _)
  io.Source.fromFile(stopwordsFile).getLines.map(getId(_)).filter(_ != -1).foreach(stopwords += _)

  // converts the string to rank
  def getId(w : String)        = vocab        getOrElse (w, -1)

  def getWord(id: Int)         = rev_vocab    getOrElse (id, "")

  def contains(id: Int)        = rev_vocab    contains  id

  def contains(w: String)      = vocab        contains  w

  def isStopWord(w: String)    = stopwords    contains  getId(w)

  def isStopWord(id: Int)      = stopwords    contains  id

  def isSpecialWord(w: String) = specialwords contains  getId(w)

  def isSpecialWord(id: Int)   = specialwords contains  id

  def getProb(id: Int)         = word_stat(id).cnt/total_words.toFloat

  //for word-stat given string
  def apply(w: Int) = word_stat(w)

  //for word-stat given id
  def apply(w: String) = word_stat(vocab(w))

  // for word-pair stat given (string, string)
  def apply(w1: Int, w2: Int) = {
    assert(isSpecialWord(w1))
    if (word_stat(w1).window_stat == null)
      word_stat(w1).window_stat = new collection.mutable.HashMap[Int, WordPairStat]
    if (word_stat(w1).window_stat.contains(w2) == false)
      word_stat(w1).window_stat += w2 -> new WordPairStat()

    word_stat(w1).window_stat(w2)
  }

  // loads the vocabulary to hash-map from string to its stat
  private def loadVocabFile(f: String): (Int, Int) = {
    var rank = 0
    var total_count = 0
    var total_uni_count = 0
    for (line <- io.Source.fromFile(f).getLines()) {
      val detail = line.stripLineEnd.split(' ')
      val word   = detail(0)
      val count  = detail(1).toInt
      if (count >= minCount) {
        vocab += (word -> rank)
        rev_vocab += (rank -> word)
        word_stat += new SingleWordStat(count)
        word_stat(rank).n_next = count
        rank += 1
      }
      total_count += count
      total_uni_count += 1
    }
    (total_count, total_uni_count)

  }

}


