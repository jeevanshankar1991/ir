package umass.ir.p1

import org.xml.sax.Attributes
import org.xml.sax.helpers.DefaultHandler
import umass.utils.VocabMaker

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

/**
 * Created by jshankar on 9/26/14.
 */
class XMLHandler(vocab_maker: VocabMaker = null, vocab_stat: VocabStat = null, corpus_stat: CorpusStat = null, make_vocab_mode:Boolean  = true)
  extends DefaultHandler {

  var page_words = new ArrayBuffer[Int]
  val book_stat  = new BookStat()
  val book_set   = new mutable.HashSet[Int]

  override def startElement(uri: String, localName: String, qName: String, attributes: Attributes): Unit = {
    qName match {
      case "page" => {
        if (make_vocab_mode == false) {
          corpus_stat.n_pages += 1
          analyzePage(page_words)
          page_words.clear
        }
      }
      case _ => {}
    }

  }

  override def endDocument(): Unit = {

    if (make_vocab_mode == false) {
      analyzePage(page_words)
      page_words.clear
      book_stat.n_uni_words = book_set.size
      for (w <- book_set) {
        vocab_stat(w).n_books += 1
      }
      book_set.clear
    }

  }

  override def characters(chars: Array[Char], start: Int, length: Int): Unit = {
      val line = WordTokenizer.cleanWord(chars, start, length)
      if (make_vocab_mode) {
         line.split(' ').filter(_.length > 0).foreach(vocab_maker += _.toLowerCase)
      } else {
          // map all the words to unique ids (in our case, it is rank) and then do all analysis from there
          page_words ++= line.split(' ').filter(_.length > 0).map(vocab_stat getId _.toLowerCase)
      }
  }

    def analyzePage(page_words: ArrayBuffer[Int], name:String = ""): Unit = {
      val page_set     = new mutable.HashSet[Int]
      val win_set      = new mutable.HashSet[Int]
      val page_stat    = new PageStat(name)
      val specialwords = vocab_stat.specialwords

      // window-level stats
      var i = 0
      var prev_word = 0
      for (words <- page_words sliding (20, 20)) {
             for (w <- words) {
               if (w != -1) {
                 win_set += w
                 page_stat.n_words += 1
               }
               if (specialwords contains w) {
                   if (i > 0 && page_words(i-1) != -1)
                     vocab_stat(w, page_words(i-1)).n_before += 1
                   if (i < page_words.size-1 && page_words(i+1) != -1)
                     vocab_stat(w, page_words(i+1)).n_after += 1
               }
               i += 1
             }
              val sp_words_win = specialwords.filter(win_set contains _)

              for (w <- win_set) {
                for (sp_word <- sp_words_win) {
                  if (sp_word != w)
                    vocab_stat(sp_word, w).n_window += 1
                }
                page_set += w
              }
              win_set.clear
      }
      // for the last word in the page, decrease the n_next stat
      if (i > 0 && page_words(i-1) != -1)
         vocab_stat.word_stat(page_words(i-1)).n_next -= 1
      assert(page_words.size == i)

      //page-level stats
      val sp_words_page = specialwords.filter(page_set contains _)
      for (w <- page_set) {
        for (sp_word <- sp_words_page) {
          if (sp_word != w)
            vocab_stat(sp_word, w).n_page += 1
        }
        vocab_stat(w).n_pages += 1
        book_set += w
      }
      page_stat.n_uni_words += page_set.size

      // book-level stat
      book_stat.n_pages     += 1
      book_stat.n_words     += page_stat.n_words
      book_stat.pages       += page_stat
      assert(book_stat.n_pages == book_stat.pages.size)

    }

}

object WordTokenizer {
  def cleanWord(chars: Array[Char], start: Int, length: Int): String = {
    var k = 0
    for (i <- start until start + length)
      if (!chars(i).isLetterOrDigit) {
        if (chars(i) != '\'' && chars(i) != '-') {
          chars(k) = ' '
          k += 1
        }
      } else {
        chars(k) = chars(i).toLower
        k += 1
      }
      new String(chars, 0, k)
    }
}


