package umass.ir.p1

import java.io.{PrintWriter, File}
import javax.xml.parsers.SAXParserFactory

import umass.utils.collections.TopN
import umass.utils.{CmdOptions, FileUtils}




/**
 * Created by jshankar on 9/27/14.
 */
object MakeStat {

   class MakeStatOptions extends CmdOptions {
     val stopwords     = new CmdOption("stopwords", "/Users/jshankar/books/stopwords.list.sorted", "STRING", "")
     val special_vocab = new CmdOption("read-special-vocab", "/Users/jshankar/books/special-words.vocab", "STRING", "")
     val user          = new CmdOption("user", "jshankar", "STRING", "")
     val books_dir     = new CmdOption("books-dir", "/Users/jshankar/books/books-tiny", "STRING", "")
     val load_vocab    = new CmdOption("read-vocab", "/Users/jshankar/books/books-tiny.vocab", "STRING", "")
     val output        = new CmdOption("output", "/Users/jshankar/books/books-tiny.ans", "STRING", "")
     val book_type     = new CmdOption("type", "tiny", "STRING", "")
     val min_count     = new CmdOption("min-count", 5, "INT", "")
   }

   def main(args : Array[String]): Unit = {
     val opts = new MakeStatOptions
     opts.parse(args)

     val vocab_stat  = new VocabStat(opts.load_vocab.value, opts.special_vocab.value, opts.stopwords.value, opts.min_count.value)
     val corpus_stat = new CorpusStat

     var cnt = 0
     FileUtils.recurseFiles(opts.books_dir.value).foreach(f => {
       corpus_stat.n_books += 1
       parseBook(f, vocab_stat, corpus_stat)
       cnt += 1
       if (cnt % 20 == 0) {
         println("Done-" + cnt)
       }
     })

     val out = new PrintWriter(new File(opts.output.value))
     val LN  = "%s %s".format(opts.user.value, opts.book_type.value)
     out.println("%s %d %d".format(LN, corpus_stat.n_books, corpus_stat.n_pages))
     out.println("%s %d".format(LN, vocab_stat.total_words))
     out.println("%s %d".format(LN, vocab_stat.total_uni_words))
     // print for top 50 words
     for (i <- 0 until 50) {
       val stat = vocab_stat(i)
       val rank = i + 1
       val tokenString = vocab_stat.getWord(i)
       val tokenBF = stat.n_books
       val tokenPF = stat.n_pages
       val tokenTF = stat.cnt
       val tokenP =  vocab_stat.getProb(i)
       val tokenPR = tokenP * rank
       out.println("%s %d %s %d %d %d %f %f".format(LN, rank, tokenString, tokenBF, tokenPF, tokenTF, tokenP, tokenPR))
     }
     // print for 7 special words
     vocab_stat.specialwords.foreach(w => {
       val stat = vocab_stat(w)
       val rank = w + 1
       val tokenString = vocab_stat.getWord(w)
       val tokenBF = stat.n_books
       val tokenPF = stat.n_pages
       val tokenTF = stat.cnt
       val tokenP  = vocab_stat.getProb(w)
       val tokenPR = tokenP * rank
       out.println("%s %d %s %d %d %d %f %f".format(LN, rank, tokenString, tokenBF, tokenPF, tokenTF, tokenP, tokenPR))
     }
     )
     // print the association words for 7 special words
     vocab_stat.specialwords.foreach(w => {
       val special_word = vocab_stat.getWord(w)
       val pageTop = new TopN[Int](10)
       val winTop  = new TopN[Int](10)
       val adjTop  = new TopN[Int](10)
       for ((other_word, word_pair_stat) <- vocab_stat(w).window_stat) {
         if (!vocab_stat.isStopWord(other_word)) {
           pageTop +=(other_word, word_pair_stat.n_page)
           winTop  +=(other_word, word_pair_stat.n_window)
           adjTop  +=(other_word, word_pair_stat.n_after)
         }
       }
       val pageTopWord = pageTop.getTopN().map(vocab_stat getWord _._1)
       val winTopWord  = winTop.getTopN().map(vocab_stat getWord _._1)
       val adjTopWord  = adjTop.getTopN().map(vocab_stat getWord _._1)
       for (i <- 0 until 5) {
         out.println("%s %s %s %s %s".format(LN, special_word, pageTopWord(i), winTopWord(i), adjTopWord(i)))
       }
     })
     out.flush
     out.close

     // association special one
     val out1 = new PrintWriter(new File(opts.output.value + ".sp"))
     vocab_stat.specialwords.foreach(w => {
         val special_word = vocab_stat.getWord(w)
         val afterTopN    = new TopN[Int](10)
         val beforeTopN   = new TopN[Int](10)
         var afterZ       = 0.0  // normalizer
         var beforeZ      = 0.0
         var logAfterZ    = 0.0
         var logBeforeZ   = 0.0
         var afterH       = 0.0  // entropy
         var beforeH      = 0.0
         for ((other_word, word_pair_stat) <- vocab_stat(w).window_stat) {
             if (!vocab_stat.isStopWord(other_word)) {
                 if (word_pair_stat.n_after > 0) {
                   afterTopN +=(other_word, word_pair_stat.n_after)
                 }
                 if (word_pair_stat.n_before > 0) {
                   beforeTopN +=(other_word, word_pair_stat.n_before)
                 }
                 afterZ     +=  word_pair_stat.n_after
                 beforeZ    +=  word_pair_stat.n_before
             }
         }
         logAfterZ  = math.log(afterZ)
         logBeforeZ = math.log(beforeZ)
         for ((other_word, word_pair_stat) <- vocab_stat(w).window_stat) {
             if (!vocab_stat.isStopWord(other_word) ) {
                   if (word_pair_stat.n_after > 0) {
                     val probAfter = word_pair_stat.n_after / afterZ
                     afterH += (-probAfter * (math.log(word_pair_stat.n_after) - logAfterZ))
                   }
                   if (word_pair_stat.n_before > 0) {
                     val probBefore = word_pair_stat.n_before / beforeZ
                     beforeH += (-probBefore * (math.log(word_pair_stat.n_before) - logBeforeZ))
                   }
           }
         }
         val afterTop10  = afterTopN.getTopN.map(id => (vocab_stat.getWord(id._1), id._2/afterZ))
         val beforeTop10 = beforeTopN.getTopN.map(id => (vocab_stat.getWord(id._1), id._2/beforeZ))
         // after
         out1.println("AFTER")
         out1.println("Z-" + afterZ)
         out1.println("ENTROPY-" + afterH)
         for (i <- 0 until 10) {
           out1.println("%s %s %s %f".format(LN, special_word, afterTop10(i)._1, afterTop10(i)._2))
         }
         out1.println("-------------\n")
         // before
         out1.println("BEFORE")
         out1.println("Z-" + beforeZ)
         out1.println("ENTROPY-" + beforeH)
         for (i <- 0 until 10) {
           out1.println("%s %s %s %f".format(LN, special_word, beforeTop10(i)._1, beforeTop10(i)._2))
         }
         out1.println("-------------\n")
     })



     out1.flush
     out1.close
   }

   def parseBook(f: File, vocab_stat: VocabStat, corpus_stat: CorpusStat): Unit = {
     val saxParser = SAXParserFactory.newInstance().newSAXParser()
     val handler = new XMLHandler(null, vocab_stat, corpus_stat, false)
     corpus_stat.n_books += 1
     try {
       saxParser.parse(f, handler)
     } catch {
       case e: Exception => println("Error in file-%s".format(f.getName))
     }
   }

}


