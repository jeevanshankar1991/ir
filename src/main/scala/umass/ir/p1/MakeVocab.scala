package umass.ir.p1

import java.io.File
import javax.xml.parsers.SAXParserFactory
import umass.utils.{VocabMaker, FileUtils, CmdOptions}
import scala.collection.mutable.HashSet



/**
 * Created by jshankar on 9/26/14.
 */
object MakeVocab {

  class MakeVocabOptions extends CmdOptions {
    val books_dir  = new CmdOption("books-dir", "/Users/jshankar/books/books-tiny", "STRING", "")
    val stopwords  = new CmdOption("stopwords", "/Users/jshankar/books/stopwords.list.sorted", "STRING", "")
    val save_vocab = new CmdOption("save-vocab", "/Users/jshankar/books/books-tiny.vocab", "STRING", "")
  }

  val vocab     = new VocabMaker()
  val stopwords = new HashSet[String]

  def main(args: Array[String]): Unit = {
    val opts = new MakeVocabOptions
    opts.parse(args)
    var cnt = 0
    FileUtils.recurseFiles(opts.books_dir.value).foreach(f => {
      parseBook(f)
      cnt += 1
      if (cnt % 20 == 0) println("Done-%d documents".format(cnt))
    })

    vocab.saveVocab(opts.save_vocab.value)
    println("Total # of Unique Words-" + vocab.size)
  }

  def parseBook(f: File): Unit = {
       val saxParser = SAXParserFactory.newInstance.newSAXParser
       val handler   = new XMLHandler(vocab, null, null, true)
       try {
         saxParser.parse(f, handler)
       } catch {
         case e: Exception => println("Error in file-%s".format(f.getName))
       }
  }
}
