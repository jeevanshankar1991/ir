package umass.ir.utils

import scala.collection.mutable.ArrayBuffer

/**
 * Created by jshankar on 9/27/14.
 */
object Sorting {


  def quickSort(a: ArrayBuffer[Int], lo: Int, hi: Int): Unit = {
    if (hi <= lo) return
    var i  = lo
    var lt = lo
    val v  = a(lo)
    var gt = hi
    while (i <= gt) {
      val cmp = a(i) - v
      if  (cmp < 0) {
        exch(a, lt, i)
        lt += 1
        i  += 1
      } else if (cmp > 0) {
        exch(a, i, gt)
        gt -= 1
      } else  {
        i += 1
      }
    }
    quickSort(a, lo, lt-1)
    quickSort(a, gt+1, hi)
  }



  private def exch(a: ArrayBuffer[Int], i: Int, j: Int) {
    val t = a(i)
    a(i) = a(j)
    a(j) = t
  }


}