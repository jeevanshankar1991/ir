package umass.ir.utils

import scala.collection.mutable.ArrayBuffer

/**
 * Created by jshankar on 9/26/14.
 */
final class RWLock {
  private val _lock = new java.util.concurrent.locks.ReentrantReadWriteLock
  @inline def readLock() { _lock.readLock().lock() }
  @inline def writeLock() { _lock.writeLock().lock() }
  @inline def readUnlock() { _lock.readLock().unlock() }
  @inline def writeUnlock() { _lock.writeLock().unlock() }
  @inline def withReadLock[T](value: => T) = {
    readLock()
    try { value } finally { readUnlock() }
  }
  @inline def withWriteLock[T](value: => T) = {
    writeLock()
    try { value } finally { writeUnlock() }
  }
}


