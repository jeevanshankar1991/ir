package umass.ir.utils

import scala.collection.mutable.ArrayBuffer
import scala.util.Random

/**
 * Created by jshankar on 9/27/14.
 */
class SlidingWindowBuffer(val window: Int = 20) {
      private val a = new ArrayBuffer[Int]
      private var k = 0
      private var currI = 0
      def apply(i: Int):Int = a(i)
      def update(i: Int, j: Int) = a(i) = j
      def size() = a.size
      def ++=(xs: TraversableOnce[Int]) = a ++= xs
      def +=(xs : Int) = a += xs
      def sort(lo: Int, hi: Int): Unit = {
         // Sorting.quickSort(a, lo, hi)
      }
      def removeDuplicates(): (Int, Int) = {
          val hi = math.min(currI + window, a.size) - 1
          var i = currI
          val prev_k  = if (k == 0) 0 else k
          // if prev_window's last word is same as a(lo), ignore until we see a new word
          if (k > 0 && a(i) == a(k)) {
              i += 1
              while (i <= hi && a(i) == a(i-1)) i += 1
          }
          while (i <= hi) {
            a(k) = a(i)
            k += 1
            i += 1
            while (i <= hi && a(i) == a(i-1)) i += 1
          }
        currI = hi
        (prev_k, k)
      }
      def test(): Unit = {
           val rng = new Random
        (0 until 5).foreach(i => {
          val n = rng.nextInt(10000)
          val a = new ArrayBuffer[Int]
          for (i <- 0 until n)
            a += rng.nextInt(100)
          //Sorting.quickSort(a, 0, n-1)
          for (i <- 0 until n-1)
            println(a(i) + " ")
          //a.foreach(x => println(" " + x))
          for (i <- 1 until n-1)
            assert(a(i) >= a(i - 1))
        })
        val x = new Array[Int](10)

      }


}
