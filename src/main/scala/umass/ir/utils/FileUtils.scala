package umass.ir.utils

import java.io.File

import scala.collection.mutable.ArrayBuffer

/**
 * Created by jshankar on 9/28/14.
 */
object FileUtils {



  // tail recursion .Given a directory, get all the files recursively in that directory
  def recurseFiles(directory: String): ArrayBuffer[File] = {

    val ans = new ArrayBuffer[File]
    recurseFiles(new File(directory), ans)
    ans
  }

  // helper function for
  private def recurseFiles(directory: File, ans: ArrayBuffer[File]): Unit = {
    if (!directory.exists()) throw new Error("File " + directory + " does not exist")

    if (directory.isFile) {
      ans += directory
    } else {
      directory.listFiles.foreach(file => {
        file.isFile match {
          case true => ans += file
          case false => recurseFiles(file, ans)
        }
      })
    }
  }

}