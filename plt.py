import matplotlib.pyplot as plt
import sys
import math

nword = 0
y = []
x = []
for line in open(sys.argv[1], 'r'):
	word,cnt = line.strip().split(' ')
	cnt      = int(cnt)
	nword   += 1
	y.append(cnt)

### plot the zips law in log-log scale
x = map(lambda ele : math.log10(ele), range(1, nword+1))
y = map(lambda ele : math.log10(ele), y)
plt.plot(x, y, '-b', label="p1")
plt.axis([0, max(x) + 5, 0, max(y) + 10])
plt.xlabel('rank of the words')
plt.ylabel('frequency of the words')
plt.title('Zips law')
plt.show()


