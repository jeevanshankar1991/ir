#!/bin/sh

workdir="/Users/jshankar/books"
stopwords="${workdir}/stopwords.list.sorted"
specialwords="${workdir}/specialwords.list.sorted"
dir="${workdir}/books-$1"


if [ ! -e ${dir}.sorted.vocab ]; then
  sbt "run-main umass.ir.p1.MakeVocab --books-dir=${dir} --save-vocab=${dir}.vocab"
  sort -nrk 2,2 ${dir}.vocab > ${dir}.sorted.vocab
  rm -rf ${dir}.vocab
fi 

## speed-increase
min_count=0
case $1 in 
   "tiny") min_count=0  ;;
   "small") min_count=0  ;;
   "medium") min_count=5 ;;
   "big")    min_count=10 ;;
esac

echo ${min_count}
sbt "run-main umass.ir.p1.MakeStat  --books-dir=${dir} --read-vocab=${dir}.sorted.vocab --min-count=${min_count} --output=${dir}.ans --type=$1 --user=jshankar --stopwords=${stopwords} --read-special-vocab=${specialwords}"
